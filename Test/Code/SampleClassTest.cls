VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SampleClassTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_SampleClass As SampleClass

'EVENTS

Private Sub Class_Initialize()

    Set m_SampleClass = New SampleClass

End Sub

Private Sub Class_Terminate()

    Set m_SampleClass = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_SampleClass

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    Dim l_EmptyArray() As Long

    This.PropertyGet("Property1") _
       .Assert(l_EmptyArray).Equals(vbArray) _
       .Assert(AnyArrayOfBoolean).Equals(vbArray) _
       .Assert(AnyArrayOfByte).Equals(vbArray) _
       .Assert(AnyArrayOfCurrency).Equals(vbArray) _
       .Assert(AnyArrayOfDate).Equals(vbArray) _
       .Assert(AnyArrayOfDouble).Equals(vbArray) _
       .Assert(AnyArrayOfInteger).Equals(vbArray) _
       .Assert(AnyArrayOfLong).Equals(vbArray) _
       .Assert(AnyArrayOfSingle).Equals(vbArray) _
       .Assert(AnyArrayOfString).Equals(vbArray) _
       .Assert(AnyArrayOfVariant).Equals(vbArray) _
       .Assert(AnyArrayOfBoolean(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfByte(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfDouble(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfInteger(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfLong(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfSingle(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfString(3&)).Equals(vbArray) _
       .Assert(AnyArrayOfVariant(3&)).Equals vbArray

    This.PropertyGet("Property1") _
       .Assert(AnyBoolean).Equals(vbBoolean) _
       .Assert(AnyByte).Equals(vbByte) _
       .Assert(AnyCurrency).Equals(vbCurrency) _
       .Assert(AnyDate).Equals(vbDate) _
       .Assert(AnyDouble).Equals(vbDouble) _
       .Assert(AnyInteger).Equals(vbInteger) _
       .Assert(AnyLong).Equals(vbLong) _
       .Assert(AnySingle).Equals(vbSingle) _
       .Assert(AnyString).Equals(vbString) _
       .Assert(AnyVariant).Greater 0&

    This.PropertyGet("Property2") _
       .Assert.IsArray

    This.PropertyGet("Property3") _
       .Assert.IsEmpty

    This.PropertyGet("Property4") _
       .Assert.IsNull

    This.PropertyGet("Property5") _
       .Assert.NotEquals vbNullString

    This.PropertyGet("Property6") _
       .Assert.Throws(5&) _
       .Assert.ThrowsAny

    This.Method("Method1") _
       .Assert.IsObjectOfType(TypeName(m_SampleClass)).PropertyGet("Property4").Assert.IsNull

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub
