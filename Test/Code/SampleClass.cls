VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SampleClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get Property1(ByRef Argument As Variant) As Variant

    If IsObject(Argument) Then

        Property1 = vbObject

    ElseIf IsArray(Argument) Then

        Property1 = vbArray

    Else

        Property1 = VarType(Argument)

    End If

End Property

Public Property Get Property2() As Variant

    Property2 = Array(1&)

End Property

Public Property Get Property3() As Variant

    Property3 = Empty

End Property

Public Property Get Property4() As Variant

    Property4 = Null

End Property

Public Property Get Property5() As String

    Property5 = "{""jsonProperty"":123}"

End Property

Public Property Get Property6() As Variant

    Err.Raise 5&

End Property

'METHODS

Public Function Method1() As Object

    Set Method1 = Me

End Function
