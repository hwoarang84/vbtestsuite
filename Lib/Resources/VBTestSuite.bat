@ECHO OFF

:: https://docs.microsoft.com/en-us/windows/win32/menurc/using-rc-the-rc-command-line-

SET BATCH_COMMAND=\Microsoft Visual Studio\VB98\Wizards\RC.EXE" "/r" "/fo" "%~dp0%~n0.res" "%~dp0%~n0.rc"
IF "%PROCESSOR_ARCHITECTURE%"=="x86" ("%PROGRAMFILES%%BATCH_COMMAND%) ELSE ("%PROGRAMFILES(x86)%%BATCH_COMMAND%)

IF %ERRORLEVEL% NEQ 0 @PAUSE