VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInProjectHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'CONSTANTS

Private Const CODE_LINE_TEMPLATE As String = "l_TestCollection.Add New "
Private Const INTERFACE_SEARCH_PATTERN As String = "Implements*"

'VARIABLES

Private m_UtilityComparer As IUtilityComparer
Private m_UtilityResource As IUtilityResource

'EVENTS

Private Sub Class_Initialize()

    Set m_UtilityComparer = New UtilityComparer
    Set m_UtilityResource = New UtilityResource

End Sub

Private Sub Class_Terminate()

    Set m_UtilityComparer = Nothing
    Set m_UtilityResource = Nothing

End Sub

'METHODS

Public Sub CreateTemporaryTestModule(ByRef Project As VBProject, ByRef ModuleName As String, ByRef Tests As Collection, ByRef Format As String)

    Dim l_Code As String
    Dim l_Item As Variant
    Dim l_Module As VBComponent

    If Not Project Is Nothing Then

        For Each l_Item In Tests
            l_Code = l_Code & CODE_LINE_TEMPLATE & l_Item & vbCrLf
        Next

        l_Code = FormatPlaceholders(m_UtilityResource.GetUTF8Content(100&), l_Code, Format)

        Set l_Module = Project.VBComponents.Add(vbext_ct_StdModule)

        With l_Module
            .CodeModule.CodePane.Window.Close
            .Name = ModuleName
            .CodeModule.AddFromString l_Code
        End With

        Set l_Module = Nothing

    End If

End Sub

Public Sub DeleteTemporaryTestModule(ByRef Project As VBProject, ByRef ModuleName As String)

    Dim l_Module As VBComponent

    On Error GoTo ErrorHandler

    If Not Project Is Nothing Then

        Set l_Module = Project.VBComponents(ModuleName)

        Project.VBComponents.Remove l_Module

    End If

ErrorHandler:

    Set l_Module = Nothing

End Sub

Public Function DoesComponentImplementInterface(ByRef Component As VBComponent, ByRef Interface As String) As Boolean

    Dim l_Iterator As Long
    Dim l_Line As String

    If Not Component Is Nothing Then

        If Component.Type = vbext_ct_ClassModule Then

            For l_Iterator = 1& To Component.CodeModule.CountOfLines

                l_Line = Component.CodeModule.Lines(l_Iterator, 1&)

                If Len(l_Line) Then

                    If l_Line Like INTERFACE_SEARCH_PATTERN & Interface Then

                        DoesComponentImplementInterface = True

                        Exit For

                    End If

                End If

            Next l_Iterator

        End If

    End If

End Function

Public Function DoesProjectReferenceExist(ByRef Project As VBProject, ByRef ReferenceName As String, ByRef ReferenceGuid As String, Optional ByVal ReferenceType As vbext_RefKind) As Boolean

    Dim l_Reference As Reference

    If Not Project Is Nothing And Len(ReferenceName) > 0& And Len(ReferenceGuid) > 0& Then

        For Each l_Reference In Project.References

            If l_Reference.Type = ReferenceType And _
               Not l_Reference.IsBroken And _
               m_UtilityComparer.AreEqual(l_Reference.Name, ReferenceName) And _
               m_UtilityComparer.AreEqual(l_Reference.Guid, ReferenceGuid) Then

                DoesProjectReferenceExist = True

                Exit For

            End If

        Next

        Set l_Reference = Nothing

    End If

End Function

Public Function GetComponentFromMethod(ByRef Project As VBProject, ByRef Method As String, Optional ByVal ComponentType As vbext_ComponentType = vbext_ct_StdModule) As String

    Dim l_Component As VBComponent

    If Not Project Is Nothing And Len(Method) > 0& Then

        For Each l_Component In Project.VBComponents

            If l_Component.Type = ComponentType Then

                If DoesComponentMemberExist(l_Component, Method) Then

                    GetComponentFromMethod = l_Component.Name

                    Exit For

                End If

            End If

        Next

        Set l_Component = Nothing

    End If

End Function

Public Sub ReplaceComponentMember(ByRef Component As VBComponent, ByRef Member As String, ByRef NewMember As String, Optional ByVal MemberType As vbext_ProcKind)

    Dim l_Line As Long
    Dim l_Signature As String

    If Not Component Is Nothing And Len(Member) > 0& And Len(NewMember) > 0& Then

        l_Line = Component.CodeModule.ProcBodyLine(Member, MemberType)
        l_Signature = Component.CodeModule.Lines(l_Line, 1&)

        Component.CodeModule.ReplaceLine l_Line, Replace(l_Signature, Member, NewMember)

    End If

End Sub

Private Function DoesComponentMemberExist(ByRef Component As VBComponent, ByRef Member As String, Optional ByVal MemberType As vbext_MemberType = vbext_mt_Method) As Boolean

    Dim l_Member As Member

    If Not Component Is Nothing And Len(Member) > 0& Then

        For Each l_Member In Component.CodeModule.Members

            If l_Member.Type = MemberType Then

                If m_UtilityComparer.AreEqual(l_Member.Name, Member) Then

                    DoesComponentMemberExist = True

                    Exit For

                End If

            End If

        Next

        Set l_Member = Nothing

    End If

End Function
