VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInIdeEventsHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInConstructible
Implements IAddInEventsHandler

'VARIABLES

Private m_AddInCallback As IAddInCallback
Private WithEvents m_BuildEvents As VBBuildEvents
Attribute m_BuildEvents.VB_VarHelpID = -1
Private m_UtilityComparer As IUtilityComparer

'EVENTS

Private Sub IAddInConstructible_Construct(Callback As IAddInCallback)

    Set m_AddInCallback = Callback
    Set m_UtilityComparer = New UtilityComparer

End Sub

Private Sub Class_Terminate()

    Set m_AddInCallback = Nothing
    Set m_BuildEvents = Nothing
    Set m_UtilityComparer = Nothing

End Sub

Private Sub m_BuildEvents_EnterDesignMode()

    If IsProjectAllowed Then m_AddInCallback.Callback EVENT_DEBUG_STOP

End Sub

Private Sub m_BuildEvents_EnterRunMode()

    If IsProjectAllowed Then m_AddInCallback.Callback EVENT_DEBUG_START

End Sub

'METHODS

Private Sub IAddInEventsHandler_Subscribe()

    Dim l_Events As Events2

    On Error GoTo ErrorHandler

    If m_AddInCallback.Context.IsModeInternal Then

        Set l_Events = m_AddInCallback.Context.Ide.Events
        Set m_BuildEvents = l_Events.VBBuildEvents

    End If

ErrorHandler:

End Sub

Private Function IsProjectAllowed() As Boolean

    On Error GoTo ErrorHandler

    IsProjectAllowed = Not m_UtilityComparer.AreEqual(m_AddInCallback.Context.Ide.VBProjects.StartProject.Name, App.EXEName)

ErrorHandler:

End Function
