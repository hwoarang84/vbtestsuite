VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInViewTestPresenter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInViewPresenter

'VARIABLES

Private m_AddInProjectHandler As IAddInProjectHandler
Private m_ListTests As ListBox
Attribute m_ListTests.VB_VarHelpID = -1
Private m_Loaded As Boolean

'EVENTS

Private Sub Class_Terminate()

    Set m_AddInProjectHandler = Nothing
    Set m_ListTests = Nothing

End Sub

Private Sub IAddInViewPresenter_Initialize(AddInView As IAddInView, Controls As Variant)

    Set m_ListTests = Controls(0)

    BindData

End Sub

'PROPERTIES

Public Property Let ProjectHandler(ByRef NewValue As IAddInProjectHandler)

    Set m_AddInProjectHandler = NewValue

End Property

'METHODS

Public Function HasSelectedItems() As Boolean

    Dim l_Iterator As Long

    If m_Loaded Then

        For l_Iterator = 0& To m_ListTests.ListCount - 1&

            If m_ListTests.Selected(l_Iterator) Then

                HasSelectedItems = True

                Exit For

            End If

        Next l_Iterator

    End If

End Function

Public Sub Run(ByRef Format As String)

    Dim l_Iterator As Long
    Dim l_Tests As Collection

    Set l_Tests = New Collection

    For l_Iterator = 0& To m_ListTests.ListCount - 1&

        If m_ListTests.Selected(l_Iterator) Then l_Tests.Add m_ListTests.List(l_Iterator)

    Next l_Iterator

    If l_Tests.Count Then m_AddInProjectHandler.PrepareProject l_Tests, Format

    Set l_Tests = Nothing

End Sub

Public Sub SetSelection(ByVal IsSelected As Boolean)

    Dim l_Iterator As Long

    If m_Loaded Then

        For l_Iterator = 0& To m_ListTests.ListCount - 1&

            m_ListTests.Selected(l_Iterator) = IsSelected

        Next l_Iterator

    End If

End Sub

Private Sub BindData()

    Dim l_Test As Variant

    For Each l_Test In m_AddInProjectHandler.GetProjectTests
        m_ListTests.AddItem l_Test
        m_ListTests.Selected(m_ListTests.ListCount - 1&) = True
    Next

    m_Loaded = True

End Sub
