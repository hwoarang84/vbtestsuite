VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IAddInProjectHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'METHODS

Public Function GetProjectTests() As Collection
    '
End Function

Public Sub PrepareProject(ByRef Tests As Collection, ByRef Format As String)
    '
End Sub

Public Sub RollbackProject()
    '
End Sub

Public Function VerifySolution() As Long
    '
End Function
