VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IAddInContextTestSuite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get IsInvoked() As Boolean
    '
End Property

Public Property Let IsInvoked(ByVal NewValue As Boolean)
    '
End Property

Public Property Get OriginalProject() As String
    '
End Property

Public Property Let OriginalProject(ByRef NewValue As String)
    '
End Property

Public Property Get TestProject() As String
    '
End Property

Public Property Let TestProject(ByRef NewValue As String)
    '
End Property

Public Property Get TestProjectEntryPoint() As String
    '
End Property

Public Property Let TestProjectEntryPoint(ByRef NewValue As String)
    '
End Property

Public Property Get TestProjectIsDirty() As Boolean
    '
End Property

Public Property Let TestProjectIsDirty(ByVal NewValue As Boolean)
    '
End Property

Public Property Get TestProjectStartModule() As String
    '
End Property

Public Property Let TestProjectStartModule(ByRef NewValue As String)
    '
End Property

Public Property Get TestProjectTemporaryModule() As String
    '
End Property

Public Property Get TestProjectTests() As Collection
    '
End Property

Public Property Let TestProjectTests(ByRef NewValue As Collection)
    '
End Property
