VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IAddInContext"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get Ide() As VBE
    '
End Property

Public Property Let Ide(ByRef NewValue As VBE)
    '
End Property

Public Property Get IsModeInternal() As Boolean
    '
End Property

Public Property Let IsModeInternal(ByVal NewValue As Boolean)
    '
End Property

Public Property Get TestSuite() As IAddInContextTestSuite
    '
End Property
