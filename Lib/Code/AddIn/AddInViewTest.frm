VERSION 5.00
Begin VB.Form AddInViewTest 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5235
   ClientLeft      =   2175
   ClientTop       =   1935
   ClientWidth     =   6570
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   204
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   HasDC           =   0   'False
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   349
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   438
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnSelect 
      Caption         =   "+"
      Height          =   300
      Left            =   3600
      TabIndex        =   2
      Top             =   120
      Width           =   330
   End
   Begin VB.CommandButton btnDeselect 
      Caption         =   "-"
      Height          =   300
      Left            =   4020
      TabIndex        =   3
      Top             =   120
      Width           =   330
   End
   Begin VB.OptionButton optFormat 
      Caption         =   "Json"
      Height          =   375
      Index           =   1
      Left            =   4560
      TabIndex        =   5
      Tag             =   "JSON"
      Top             =   960
      Width           =   1815
   End
   Begin VB.OptionButton optFormat 
      Caption         =   "Html"
      Height          =   375
      Index           =   0
      Left            =   4560
      TabIndex        =   4
      Tag             =   "HTML"
      Top             =   480
      Value           =   -1  'True
      Width           =   1815
   End
   Begin VB.ListBox lstTests 
      Height          =   4620
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   1
      Top             =   480
      Width           =   4215
   End
   Begin VB.CommandButton btnRun 
      Caption         =   "Run Tests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4560
      TabIndex        =   0
      Top             =   4605
      Width           =   1815
   End
   Begin VB.Label lblFormat 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Output format:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   4560
      TabIndex        =   7
      Top             =   120
      Width           =   1200
   End
   Begin VB.Label lblTests 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Select tests:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "AddInViewTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInView

'VARIABLES

Private m_Presenter As IAddInViewPresenter

'EVENTS

Private Sub Form_Initialize()

    Set m_Presenter = New AddInViewTestPresenter

End Sub

Private Sub Form_Load()

    Dim l_HasSelectedItems As Boolean

    Me.Caption = App.FileDescription & ChrW$(32&) & App.Major & ChrW$(46&) & App.Minor & ChrW$(46&) & App.Revision

    m_Presenter.Initialize Me, Array(lstTests)

    l_HasSelectedItems = Presenter.HasSelectedItems

    btnDeselect.Enabled = l_HasSelectedItems
    btnRun.Enabled = l_HasSelectedItems
    btnSelect.Enabled = l_HasSelectedItems

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Set m_Presenter = Nothing

End Sub

Private Sub IAddInView_Callback()
    '
End Sub

Private Sub btnDeselect_Click()

    Presenter.SetSelection False

End Sub

Private Sub btnRun_Click()

    Dim l_Option As OptionButton
    Dim l_Tag As String

    For Each l_Option In optFormat

        If l_Option.Value Then

            l_Tag = l_Option.Tag

            Exit For

        End If

    Next

    Set l_Option = Nothing

    Presenter.Run l_Tag

    Unload Me

End Sub

Private Sub btnSelect_Click()

    Presenter.SetSelection True

End Sub

Private Sub lstTests_ItemCheck(Item As Integer)

    btnRun.Enabled = Presenter.HasSelectedItems

End Sub

'PROPERTIES

Private Property Get Presenter() As AddInViewTestPresenter

    Set Presenter = m_Presenter

End Property

Public Property Let ProjectHandler(ByRef NewValue As IAddInProjectHandler)

    Presenter.ProjectHandler = NewValue

End Property
