VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInFileEventsHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInConstructible
Implements IAddInEventsHandler

'VARIABLES

Private m_AddInCallback As IAddInCallback
Private WithEvents m_FileEvents As FileControlEvents
Attribute m_FileEvents.VB_VarHelpID = -1
Private m_UtilityFileSystem As IUtilityFileSystem

'EVENTS

Private Sub IAddInConstructible_Construct(Callback As IAddInCallback)

    Set m_AddInCallback = Callback
    Set m_UtilityFileSystem = New UtilityFileSystem

End Sub

Private Sub Class_Terminate()

    Set m_AddInCallback = Nothing
    Set m_FileEvents = Nothing
    Set m_UtilityFileSystem = Nothing

End Sub

Private Sub m_FileEvents_AfterWriteFile(ByVal Project As VBProject, ByVal FileType As vbext_FileType, ByVal FileName As String, ByVal Result As Integer)

    Dim l_Path As String
    Dim l_Position As Long

    If IsTemporaryFile(FileName) Then

        l_Path = Project.FileName
        l_Position = InStrRev(l_Path, CHAR_SLASH, , vbBinaryCompare)

        m_UtilityFileSystem.Delete Left$(l_Path, l_Position) & FileName

    End If

End Sub

'METHODS

Private Sub IAddInEventsHandler_Subscribe()

    On Error GoTo ErrorHandler

    If m_AddInCallback.Context.IsModeInternal Then Set m_FileEvents = m_AddInCallback.Context.Ide.Events.FileControlEvents(m_AddInCallback.Context.Ide.ActiveVBProject)

ErrorHandler:

End Sub

Private Function IsTemporaryFile(ByRef FileName As String) As Boolean

    On Error GoTo ErrorHandler

    IsTemporaryFile = InStr(1&, FileName, m_AddInCallback.Context.TestSuite.TestProjectTemporaryModule, vbBinaryCompare)

ErrorHandler:

End Function
