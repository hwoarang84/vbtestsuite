VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInProjectHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInConstructible
Implements IAddInProjectHandler

'ENUMERATIOS

Public Enum ENUM_VERIFY_PROJECT
    VERIFY_PROJECT_OK&
    VERIFY_PROJECT_NOPROJECT&
    VERIFY_PROJECT_NOREFERENCE&
    VERIFY_PROJECT_NOTESTS&
End Enum

'CONSTANTS

Private Const DLL_GUID As String = "{CB658DA2-2811-4F84-B2BA-BC518F3FAD98}"
Private Const SUB_MAIN As String = "Main"
Private Const SUB_MAIN_DISABLED As String = SUB_MAIN & "DisabledTemp"
Private Const TEST_INTERFACE As String = "ITest"

'VARIABLES

Private m_AddInCallback As IAddInCallback
Private m_AddInProjectHelper As AddInProjectHelper
Private m_UtilityComparer As IUtilityComparer

'EVENTS

Private Sub IAddInConstructible_Construct(Callback As IAddInCallback)

    Set m_AddInCallback = Callback
    Set m_AddInProjectHelper = New AddInProjectHelper
    Set m_UtilityComparer = New UtilityComparer

End Sub

Private Sub Class_Terminate()

    Set m_AddInCallback = Nothing
    Set m_AddInProjectHelper = Nothing
    Set m_UtilityComparer = Nothing

End Sub

'METHODS

Private Function IAddInProjectHandler_GetProjectTests() As Collection

    Set IAddInProjectHandler_GetProjectTests = GetProjectTests(m_AddInCallback.Context.Ide.VBProjects(m_AddInCallback.Context.TestSuite.TestProject))

End Function

Private Sub IAddInProjectHandler_PrepareProject(Tests As Collection, Format As String)

    Dim l_DebugButton As Object

    Set l_DebugButton = m_AddInCallback.Context.Ide.CommandBars.FindControl(1&, 186&, , True) 'msoControlButton

    If Not l_DebugButton Is Nothing Then

        With m_AddInCallback.Context.TestSuite
            .IsInvoked = True
            .OriginalProject = m_AddInCallback.Context.Ide.VBProjects.StartProject.Name
            .TestProjectIsDirty = m_AddInCallback.Context.Ide.VBProjects(m_AddInCallback.Context.TestSuite.TestProject).IsDirty
            .TestProjectTests = Tests
        End With

        If Not m_UtilityComparer.AreEqual(m_AddInCallback.Context.TestSuite.TestProject, m_AddInCallback.Context.TestSuite.OriginalProject) Then

            m_AddInCallback.Context.Ide.VBProjects.StartProject = m_AddInCallback.Context.Ide.VBProjects(m_AddInCallback.Context.TestSuite.TestProject)

        End If

        If IsObject(m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents.StartUpObject) Then

            m_AddInCallback.Context.TestSuite.TestProjectEntryPoint = m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents.StartUpObject.Name
            m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents.StartUpObject = 0&

        Else

            m_AddInCallback.Context.TestSuite.TestProjectEntryPoint = vbNullString

        End If

        m_AddInCallback.Context.TestSuite.TestProjectStartModule = m_AddInProjectHelper.GetComponentFromMethod(m_AddInCallback.Context.Ide.VBProjects.StartProject, SUB_MAIN)

        If Len(m_AddInCallback.Context.TestSuite.TestProjectStartModule) Then

            m_AddInProjectHelper.ReplaceComponentMember m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents(m_AddInCallback.Context.TestSuite.TestProjectStartModule), SUB_MAIN, SUB_MAIN_DISABLED

        End If

        m_AddInProjectHelper.CreateTemporaryTestModule m_AddInCallback.Context.Ide.VBProjects.StartProject, m_AddInCallback.Context.TestSuite.TestProjectTemporaryModule, m_AddInCallback.Context.TestSuite.TestProjectTests, Format

        l_DebugButton.Execute

    End If

End Sub

Private Sub IAddInProjectHandler_RollbackProject()

    If m_AddInCallback.Context.TestSuite.IsInvoked Then

        m_AddInProjectHelper.DeleteTemporaryTestModule m_AddInCallback.Context.Ide.VBProjects.StartProject, m_AddInCallback.Context.TestSuite.TestProjectTemporaryModule

        If Len(m_AddInCallback.Context.TestSuite.TestProjectStartModule) Then

            m_AddInProjectHelper.ReplaceComponentMember m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents(m_AddInCallback.Context.TestSuite.TestProjectStartModule), SUB_MAIN_DISABLED, SUB_MAIN

        End If

        If Len(m_AddInCallback.Context.TestSuite.TestProjectEntryPoint) Then

            m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents.StartUpObject = m_AddInCallback.Context.Ide.VBProjects.StartProject.VBComponents(m_AddInCallback.Context.TestSuite.TestProjectEntryPoint)

        End If

        If Not m_AddInCallback.Context.TestSuite.TestProjectIsDirty Then m_AddInCallback.Context.Ide.VBProjects.StartProject.IsDirty = False

        If Not m_UtilityComparer.AreEqual(m_AddInCallback.Context.TestSuite.TestProject, m_AddInCallback.Context.TestSuite.OriginalProject) Then

            m_AddInCallback.Context.Ide.VBProjects.StartProject = m_AddInCallback.Context.Ide.VBProjects(m_AddInCallback.Context.TestSuite.OriginalProject)

        End If

        With m_AddInCallback.Context.TestSuite
            .IsInvoked = False
            .OriginalProject = vbNullString
            .TestProject = vbNullString
            .TestProjectEntryPoint = vbNullString
            .TestProjectIsDirty = False
            .TestProjectStartModule = vbNullString
            .TestProjectTests = Nothing
        End With

    End If

End Sub

Private Function IAddInProjectHandler_VerifySolution() As Long

    Dim l_Project As VBProject
    Dim l_ProjectCount As Long

    m_AddInCallback.Context.TestSuite.TestProject = vbNullString

    If m_AddInCallback.Context.Ide.VBProjects.Count Then

        For Each l_Project In m_AddInCallback.Context.Ide.VBProjects

            If l_Project.Type = vbext_pt_StandardExe Then

                If m_AddInProjectHelper.DoesProjectReferenceExist(l_Project, App.Title, DLL_GUID) Then

                    If GetProjectTests(l_Project).Count Then m_AddInCallback.Context.TestSuite.TestProject = l_Project.Name Else IAddInProjectHandler_VerifySolution = VERIFY_PROJECT_NOTESTS

                    Exit Function

                End If

                l_ProjectCount = l_ProjectCount + 1&

            End If

        Next

        If l_ProjectCount Then IAddInProjectHandler_VerifySolution = VERIFY_PROJECT_NOREFERENCE Else IAddInProjectHandler_VerifySolution = VERIFY_PROJECT_NOPROJECT

    Else

        IAddInProjectHandler_VerifySolution = VERIFY_PROJECT_NOPROJECT

    End If

End Function

Private Function GetProjectTests(Project As VBProject) As Collection

    Dim l_Component As VBComponent
    Dim l_Tests As Collection

    Set l_Tests = New Collection

    If Not Project Is Nothing Then

        For Each l_Component In Project.VBComponents

            If m_AddInProjectHelper.DoesComponentImplementInterface(l_Component, TEST_INTERFACE) Then l_Tests.Add l_Component.Name, l_Component.Name

        Next

        Set l_Component = Nothing

    End If

    Set GetProjectTests = l_Tests

    Set l_Tests = Nothing

End Function
