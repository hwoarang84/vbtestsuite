VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInConnector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInCallback
Implements IAddInConnector

'ENUMERATIOS

Public Enum ENUM_EVENT
    EVENT_DEBUG_START&
    EVENT_DEBUG_STOP&
    EVENT_MENU_CLICK&
End Enum

'VARIABLES

Private m_AddInContext As IAddInContext
Private m_AddInFileEventsHandler As IAddInEventsHandler
Private m_AddInIdeEventsHandler As IAddInEventsHandler
Private m_AddInMenuEventsHandler As IAddInEventsHandler
Private m_AddInProjectHandler As IAddInProjectHandler

'EVENTS

Private Sub Class_Initialize()

    Set m_AddInContext = New AddInContext
    Set m_AddInFileEventsHandler = New AddInFileEventsHandler
    Set m_AddInIdeEventsHandler = New AddInIdeEventsHandler
    Set m_AddInMenuEventsHandler = New AddInMenuEventsHandler
    Set m_AddInProjectHandler = New AddInProjectHandler

End Sub

Private Sub Class_Terminate()

    Set m_AddInContext = Nothing
    Set m_AddInFileEventsHandler = Nothing
    Set m_AddInIdeEventsHandler = Nothing
    Set m_AddInMenuEventsHandler = Nothing
    Set m_AddInProjectHandler = Nothing

End Sub

'PROPERTIES

Private Property Get IAddInCallback_Context() As IAddInContext

    Set IAddInCallback_Context = m_AddInContext

End Property

'METHODS

Private Sub IAddInCallback_Callback(ByVal Parameter As Long)

    Select Case Parameter

        Case EVENT_DEBUG_STOP

            m_AddInProjectHandler.RollbackProject

        Case EVENT_MENU_CLICK

            Select Case m_AddInProjectHandler.VerifySolution

                Case VERIFY_PROJECT_OK

                    ShowViewAddInTest m_AddInProjectHandler

                Case VERIFY_PROJECT_NOPROJECT

                    Dialog_MessageBox ERR_MSG_ADDIN_NOPROJECT, MB_ICONSTOP, App.FileDescription

                Case VERIFY_PROJECT_NOREFERENCE

                    Dialog_MessageBox FormatPlaceholders(ERR_MSG_ADDIN_PROJECT_NOREFERENCE, App.FileDescription), MB_ICONSTOP, App.FileDescription

                Case VERIFY_PROJECT_NOTESTS

                    Dialog_MessageBox FormatPlaceholders(ERR_MSG_ADDIN_PROJECT_NOTESTS, App.EXEName), MB_ICONEXCLAMATION, App.FileDescription

            End Select

    End Select

End Sub

Private Sub IAddInConnector_Connect(Ide As VBE, ByVal Mode As Long)

    With m_AddInContext
        .Ide = Ide
        .IsModeInternal = Mode <> 2& 'ext_cm_External
    End With

    ConstructEventHandler m_AddInFileEventsHandler
    ConstructEventHandler m_AddInIdeEventsHandler
    ConstructEventHandler m_AddInMenuEventsHandler

    ConstructObject m_AddInProjectHandler

End Sub

Private Sub IAddInConnector_Disconnect(ByVal Mode As Long)
    '
End Sub

Private Sub ConstructEventHandler(ByRef Object As IAddInEventsHandler)

    If ConstructObject(Object) Then Object.Subscribe

End Sub

Private Function ConstructObject(ByRef Object As Object) As Boolean

    Dim l_Constructible As IAddInConstructible

    If Not Object Is Nothing Then

        If TypeOf Object Is IAddInConstructible Then

            Set l_Constructible = Object

            l_Constructible.Construct Me

            Set l_Constructible = Nothing

            ConstructObject = True

        End If

    End If

End Function

Private Sub ShowViewAddInTest(ByRef ProjectHandler As IAddInProjectHandler)

    On Error GoTo ErrorHandler

    With AddInViewTest
        .ProjectHandler = ProjectHandler
        .Show vbModal
    End With

ErrorHandler:

    Set AddInViewTest = Nothing

End Sub
