VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInMenuEventsHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInConstructible
Implements IAddInEventsHandler

'CONSTANTS

Private Const ADDINS_COMMANDBAR As String = "Add-Ins"

'VARIABLES

Private m_AddInCallback As IAddInCallback
Private m_AddInMenu As Object
Private WithEvents m_CommandBarEvents As CommandBarEvents
Attribute m_CommandBarEvents.VB_VarHelpID = -1
Private m_UtilityComparer As IUtilityComparer

'EVENTS

Private Sub IAddInConstructible_Construct(Callback As IAddInCallback)

    Set m_AddInCallback = Callback
    Set m_UtilityComparer = New UtilityComparer

End Sub

Private Sub Class_Terminate()

    On Error Resume Next

    Set m_AddInCallback = Nothing

    If Not m_AddInMenu Is Nothing Then

        m_AddInMenu.Delete

        Set m_AddInMenu = Nothing

    End If

    Set m_CommandBarEvents = Nothing
    Set m_UtilityComparer = Nothing

End Sub

Private Sub m_CommandBarEvents_Click(ByVal CommandBarControl As Object, IsHandled As Boolean, CancelDefault As Boolean)

    If IsProjectAllowed Then m_AddInCallback.Callback EVENT_MENU_CLICK

End Sub

'METHODS

Private Sub IAddInEventsHandler_Subscribe()

    Dim l_CommandBar As Object

    On Error GoTo ErrorHandler

    If m_AddInCallback.Context.IsModeInternal Then

        Set l_CommandBar = m_AddInCallback.Context.Ide.CommandBars(ADDINS_COMMANDBAR)

        If Not l_CommandBar Is Nothing Then

            Set m_AddInMenu = l_CommandBar.Controls.Add(1&)
            m_AddInMenu.Caption = App.FileDescription

            Set m_CommandBarEvents = m_AddInCallback.Context.Ide.Events.CommandBarEvents(m_AddInMenu)

            Set l_CommandBar = Nothing

        End If

    End If

ErrorHandler:

End Sub

Private Function IsProjectAllowed() As Boolean

    On Error GoTo ErrorHandler

    IsProjectAllowed = Not m_UtilityComparer.AreEqual(m_AddInCallback.Context.Ide.VBProjects.StartProject.Name, App.EXEName)

ErrorHandler:

End Function
