VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInContextTestSuite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInContextTestSuite

'CONSTANTS

Private Const TEMP_MODULE_POSTFIX As String = "ModTemp"

'VARIABLES

Private m_IsInvoked As Boolean
Private m_OriginalProject As String
Private m_TestProject As String
Private m_TestProjectEntryPoint As String
Private m_TestProjectIsDirty As Boolean
Private m_TestProjectStartModule As String
Private m_TestProjectTests As Collection

'EVENTS

Private Sub Class_Terminate()

    Set m_TestProjectTests = Nothing

End Sub

'PROPERTIES

Private Property Get IAddInContextTestSuite_IsInvoked() As Boolean

    IAddInContextTestSuite_IsInvoked = m_IsInvoked

End Property

Private Property Let IAddInContextTestSuite_IsInvoked(ByVal RHS As Boolean)

    m_IsInvoked = RHS

End Property

Private Property Get IAddInContextTestSuite_OriginalProject() As String

    IAddInContextTestSuite_OriginalProject = m_OriginalProject

End Property

Private Property Let IAddInContextTestSuite_OriginalProject(RHS As String)

    m_OriginalProject = RHS

End Property

Private Property Get IAddInContextTestSuite_TestProject() As String

    IAddInContextTestSuite_TestProject = m_TestProject

End Property

Private Property Let IAddInContextTestSuite_TestProject(RHS As String)

    m_TestProject = RHS

End Property

Private Property Get IAddInContextTestSuite_TestProjectEntryPoint() As String

    IAddInContextTestSuite_TestProjectEntryPoint = m_TestProjectEntryPoint

End Property

Private Property Let IAddInContextTestSuite_TestProjectEntryPoint(RHS As String)

    m_TestProjectEntryPoint = RHS

End Property

Private Property Get IAddInContextTestSuite_TestProjectIsDirty() As Boolean

    IAddInContextTestSuite_TestProjectIsDirty = m_TestProjectIsDirty

End Property

Private Property Let IAddInContextTestSuite_TestProjectIsDirty(ByVal RHS As Boolean)

    m_TestProjectIsDirty = RHS

End Property

Private Property Get IAddInContextTestSuite_TestProjectStartModule() As String

    IAddInContextTestSuite_TestProjectStartModule = m_TestProjectStartModule

End Property

Private Property Let IAddInContextTestSuite_TestProjectStartModule(RHS As String)

    m_TestProjectStartModule = RHS

End Property

Private Property Get IAddInContextTestSuite_TestProjectTemporaryModule() As String

    IAddInContextTestSuite_TestProjectTemporaryModule = App.Title & TEMP_MODULE_POSTFIX

End Property

Private Property Get IAddInContextTestSuite_TestProjectTests() As Collection

    Set IAddInContextTestSuite_TestProjectTests = m_TestProjectTests

End Property

Private Property Let IAddInContextTestSuite_TestProjectTests(RHS As Collection)

    Set m_TestProjectTests = RHS

End Property
