VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AddInContext"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IAddInContext

'VARIABLES

Private m_AddInContextTestSuite As IAddInContextTestSuite
Private m_Ide As VBE
Private m_IsModeInternal As Boolean

'EVENTS

Private Sub Class_Initialize()

    Set m_AddInContextTestSuite = New AddInContextTestSuite

End Sub

Private Sub Class_Terminate()

    Set m_AddInContextTestSuite = Nothing
    Set m_Ide = Nothing

End Sub

'PROPERTIES

Private Property Get IAddInContext_Ide() As VBE

    Set IAddInContext_Ide = m_Ide

End Property

Private Property Let IAddInContext_Ide(RHS As VBE)

    Set m_Ide = RHS

End Property

Private Property Get IAddInContext_IsModeInternal() As Boolean

    IAddInContext_IsModeInternal = m_IsModeInternal

End Property

Private Property Let IAddInContext_IsModeInternal(ByVal RHS As Boolean)

    m_IsModeInternal = RHS

End Property

Private Property Get IAddInContext_TestSuite() As IAddInContextTestSuite

    Set IAddInContext_TestSuite = m_AddInContextTestSuite

End Property
