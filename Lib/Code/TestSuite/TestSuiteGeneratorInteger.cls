VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteGeneratorInteger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteGenerator

'CONSTANTS

Private Const INT_MAX As Double = 32767#
Private Const INT_MIN As Double = -32768#

'METHODS

Private Function ITestSuiteGenerator_GetValue(ByVal Length As Long) As Variant

    Randomize

    ITestSuiteGenerator_GetValue = CInt(INT_MIN + Int((Abs(INT_MAX - INT_MIN) + 1) * Rnd))

End Function
