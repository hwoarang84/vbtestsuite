VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteRecorder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteCallback
Implements ITestSuiteRecorder

'CONSTANTS

Private Const DATETIME_FORMAT As String = "yyyy-mm-dd-hh-mm-ss"

'VARIABLES

Private m_Buffer As String
Private m_TestSuite As ITestSuiteConstructible
Private m_TestSuiteFormatter As ITestSuiteFormatter
Private m_TestSuiteInvoker As ITestSuiteInvoker
Private m_TestsCountFailed As Long
Private m_TestsCountSuccess As Long
Private m_UtilityFileSystem As IUtilityFileSystem

'EVENTS

Private Sub Class_Terminate()

    Set m_TestSuite = Nothing
    Set m_TestSuiteFormatter = Nothing
    Set m_TestSuiteInvoker = Nothing
    Set m_UtilityFileSystem = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteCallback_Invoker() As ITestSuiteInvoker

    Set ITestSuiteCallback_Invoker = m_TestSuiteInvoker

End Property

'METHODS

Private Sub ITestSuiteCallback_Callback()

    If m_TestSuiteInvoker.Context.HasFailed Then m_TestsCountFailed = m_TestsCountFailed + 1& Else m_TestsCountSuccess = m_TestsCountSuccess + 1&

    m_Buffer = m_Buffer & m_TestSuiteFormatter.Format(m_TestSuiteInvoker.Context) & m_TestSuiteFormatter.Separator

End Sub

Private Sub ITestSuiteRecorder_Record(Tests As Collection, ByVal Format As Long, FileName As String)

    Dim l_FileName As String
    Dim l_ObjectName As String
    Dim l_Template As String
    Dim l_Test As ITest

    On Error GoTo ErrorHandler

    SetupTestSuite
    SetupTestSuiteFormatter Format
    SetupTestSuiteInvoker
    SetupUtilityFileSystem

    For Each l_Test In Tests

        l_ObjectName = TypeName(l_Test.Object)

        m_Buffer = vbNullString
        m_TestsCountFailed = 0&
        m_TestsCountSuccess = 0&

        m_TestSuite.Construct Me, l_Test
        l_Test.Test m_TestSuite

        l_Template = FormatPlaceholders(m_TestSuiteFormatter.Body, _
           l_ObjectName, _
           m_TestsCountSuccess, _
           m_TestsCountFailed, _
           Left$(m_Buffer, Len(m_Buffer) - Len(m_TestSuiteFormatter.Separator)))

        l_FileName = IIf(Len(FileName), FileName, App.EXEName) & ChrW$(95&) & l_ObjectName & ChrW$(95&) & Strings.Format$(Now, DATETIME_FORMAT) & m_TestSuiteFormatter.Extension

        If m_UtilityFileSystem.Create(l_FileName) Then

            m_UtilityFileSystem.WriteUTF8 m_TestSuiteFormatter.Header
            m_UtilityFileSystem.WriteUTF8 l_Template
            m_UtilityFileSystem.WriteUTF8 m_TestSuiteFormatter.Footer

            m_UtilityFileSystem.Flush

            m_UtilityFileSystem.Execute l_FileName

        End If

    Next

ErrorHandler:

    ThrowIfAny

End Sub

Private Sub SetupTestSuite()

    If Not m_TestSuite Is Nothing Then Set m_TestSuite = Nothing

    Set m_TestSuite = New TestSuite

End Sub

Private Sub SetupTestSuiteFormatter(ByVal Format As Long)

    If Not m_TestSuiteFormatter Is Nothing Then Set m_TestSuiteFormatter = Nothing

    Select Case Format

        Case JSON: Set m_TestSuiteFormatter = New TestSuiteFormatterJson

        Case Else: Set m_TestSuiteFormatter = New TestSuiteFormatterHtml

    End Select

End Sub

Private Sub SetupTestSuiteInvoker()

    If Not m_TestSuiteInvoker Is Nothing Then Set m_TestSuiteInvoker = Nothing

    Set m_TestSuiteInvoker = New TestSuiteInvoker

End Sub

Private Sub SetupUtilityFileSystem()

    If Not m_UtilityFileSystem Is Nothing Then Set m_UtilityFileSystem = Nothing

    Set m_UtilityFileSystem = New UtilityFileSystem

End Sub
