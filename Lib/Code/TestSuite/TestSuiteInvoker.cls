VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteInvoker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteInvoker

'VARIABLES

Private m_TestSuiteInvokerContext As ITestSuiteInvokerContext
Private m_TLIApplication As Object
Private m_TLIInvocationId As Long
Private m_UtilityTimer As IUtilityTimer

'CONSTANTS

Private Const TLI_APPLICATION As String = "TLI.TLIApplication"

'EVENTS

Private Sub Class_Initialize()

    On Error GoTo ErrorHandler

    Set m_TestSuiteInvokerContext = New TestSuiteInvokerContext
    Set m_TLIApplication = CreateObject(TLI_APPLICATION)
    Set m_UtilityTimer = New UtilityTimer

    Exit Sub

ErrorHandler:

    Throw ERR_ID_TLI_APPLICATION_CREATE, ERR_MSG_TLI_APPLICATION_CREATE, TLI_APPLICATION

End Sub

Private Sub Class_Terminate()

    Set m_TestSuiteInvokerContext = Nothing
    Set m_TLIApplication = Nothing
    Set m_UtilityTimer = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteInvoker_Context() As ITestSuiteInvokerContext

    Set ITestSuiteInvoker_Context = m_TestSuiteInvokerContext

End Property

'METHODS

Private Sub ITestSuiteInvoker_GetMember(Object As Object, Member As String, ByVal CallType As Long)

    If Object Is Nothing Then Throw ERR_ID_TLI_TESTABLE_OBJECT_NOTHING, ERR_MSG_TLI_TESTABLE_OBJECT_NOTHING

    On Error GoTo ErrorHandler

    With m_TestSuiteInvokerContext
        .CallTime = 0#
        .CallType = CallType
        .Member = Member
    End With

    m_TLIInvocationId = m_TLIApplication.InvokeId(Object, Member)

ErrorHandler:

    HandleError

End Sub

Private Sub ITestSuiteInvoker_Invoke(Object As Object, Arguments As Variant)

    Dim l_Args() As Variant
    Dim l_ArgsCount As Long
    Dim l_Iterator As Long

    If Object Is Nothing Then Throw ERR_ID_TLI_TESTABLE_OBJECT_NOTHING, ERR_MSG_TLI_TESTABLE_OBJECT_NOTHING

    On Error GoTo ErrorHandler

    With m_TestSuiteInvokerContext
        .Actual = Empty
        .Arguments = Empty
        .Check = vbNullString
        .ErrDescription = vbNullString
        .ErrNumber = 0&
        .Expected = Empty
        .HasFailed = False
    End With

    l_ArgsCount = UBound(Arguments)

    If l_ArgsCount >= 0& Then

        m_TestSuiteInvokerContext.Arguments = Arguments

        ReDim l_Args(l_ArgsCount)

        For l_Iterator = 0& To l_ArgsCount

            If IsObject(Arguments(l_ArgsCount - l_Iterator)) Then Set l_Args(l_Iterator) = Arguments(l_ArgsCount - l_Iterator) Else l_Args(l_Iterator) = Arguments(l_ArgsCount - l_Iterator)

        Next l_Iterator

        m_UtilityTimer.Start

        m_TestSuiteInvokerContext.Actual = m_TLIApplication.InvokeHookArray(Object, m_TLIInvocationId, m_TestSuiteInvokerContext.CallType, l_Args)
        m_TestSuiteInvokerContext.CallTime = m_UtilityTimer.Elapsed

        Erase l_Args

    Else

        m_UtilityTimer.Start

        m_TestSuiteInvokerContext.Actual = m_TLIApplication.InvokeHook(Object, m_TLIInvocationId, m_TestSuiteInvokerContext.CallType)
        m_TestSuiteInvokerContext.CallTime = m_UtilityTimer.Elapsed

    End If

ErrorHandler:

    HandleError

End Sub

Private Sub HandleError()

    If Err.Number Then

        With m_TestSuiteInvokerContext
            .ErrDescription = Err.Description
            .ErrNumber = Err.Number
        End With

    End If

End Sub
