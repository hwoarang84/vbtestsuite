VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuite
Implements ITestSuiteCallback
Implements ITestSuiteConstructible

'VARIABLES

Private m_TestSuiteAction As ITestSuiteConstructible
Private m_TestSuiteCallback As ITestSuiteCallback
Private m_Test As ITest

'EVENTS

Private Function ITestSuiteConstructible_Construct(Callback As ITestSuiteCallback, Test As ITest) As ITestSuiteConstructible

    Class_Terminate

    Set m_TestSuiteAction = New TestSuiteAction
    Set m_TestSuiteCallback = Callback
    Set m_Test = Test

    Set ITestSuiteConstructible_Construct = Me

End Function

Private Sub Class_Terminate()

    Set m_TestSuiteAction = Nothing
    Set m_TestSuiteCallback = Nothing
    Set m_Test = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteCallback_Invoker() As ITestSuiteInvoker

    Set ITestSuiteCallback_Invoker = m_TestSuiteCallback.Invoker

End Property

'METHODS

Private Function ITestSuite_Method(Name As String) As ITestSuiteAction

    Set ITestSuite_Method = GetFrameworkAction(Name, VbMethod)

End Function

Private Function ITestSuite_PropertyGet(Name As String) As ITestSuiteAction

    Set ITestSuite_PropertyGet = GetFrameworkAction(Name, VbGet)

End Function

Private Function ITestSuite_PropertyLet(Name As String) As ITestSuiteAction

    Set ITestSuite_PropertyLet = GetFrameworkAction(Name, VbLet)

End Function

Private Function ITestSuite_PropertySet(Name As String) As ITestSuiteAction

    Set ITestSuite_PropertySet = GetFrameworkAction(Name, VbSet)

End Function

Private Sub ITestSuiteCallback_Callback()

    m_TestSuiteCallback.Callback

End Sub

Private Function GetFrameworkAction(ByRef Member As String, ByVal CallType As Long) As ITestSuiteAction

    m_TestSuiteCallback.Invoker.GetMember m_Test.Object, Member, CallType

    Set GetFrameworkAction = m_TestSuiteAction.Construct(Me, m_Test)

End Function
