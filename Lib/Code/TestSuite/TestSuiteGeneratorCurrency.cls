VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteGeneratorCurrency"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteGenerator

'CONSTANTS

Private Const CURRENCY_MAX As Double = 922337203685477.5807@
Private Const CURRENCY_MIN As Double = -922337203685477.5807@

'METHODS

Private Function ITestSuiteGenerator_GetValue(ByVal Length As Long) As Variant

    Randomize

    ITestSuiteGenerator_GetValue = CCur(CURRENCY_MIN + Int((Abs(CURRENCY_MAX - CURRENCY_MIN) + 1) * Rnd))

End Function
