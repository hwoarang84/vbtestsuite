VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteGeneratorDouble"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteGenerator

'CONSTANTS

Private Const DOUBLE_MAX As Double = 1.79769313486231E+308
Private Const DOUBLE_MIN As Double = -1.79769313486231E+308

'METHODS

Private Function ITestSuiteGenerator_GetValue(ByVal Length As Long) As Variant

    Randomize

    ITestSuiteGenerator_GetValue = CDbl(DOUBLE_MIN + Int((Abs(DOUBLE_MAX - DOUBLE_MIN) + 1) * Rnd))

End Function
