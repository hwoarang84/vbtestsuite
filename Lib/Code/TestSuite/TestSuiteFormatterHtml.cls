VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteFormatterHtml"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteFormatter

'CONSTANTS

Private Const COLOR_GREEN As String = "green"
Private Const COLOR_RED As String = "red"

'VARIABLES

Private m_TestSuiteFormatterBase As TestSuiteFormatterBase

'EVENTS

Private Sub Class_Initialize()

    Set m_TestSuiteFormatterBase = New TestSuiteFormatterBase

End Sub

Private Sub Class_Terminate()

    Set m_TestSuiteFormatterBase = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteFormatter_Body() As String

    ITestSuiteFormatter_Body = m_TestSuiteFormatterBase.GetBody(12&)

End Property

Private Property Get ITestSuiteFormatter_Extension() As String

    ITestSuiteFormatter_Extension = ".html"

End Property

Private Property Get ITestSuiteFormatter_Footer() As String

    ITestSuiteFormatter_Footer = m_TestSuiteFormatterBase.GetFooter(11&)

End Property

Private Property Get ITestSuiteFormatter_Header() As String

    ITestSuiteFormatter_Header = m_TestSuiteFormatterBase.GetHeader(10&)

End Property

Private Property Get ITestSuiteFormatter_Separator() As String

    ITestSuiteFormatter_Separator = vbNullString

End Property

'METHODS

Private Function ITestSuiteFormatter_Format(InvokerContext As ITestSuiteInvokerContext) As String

    ITestSuiteFormatter_Format = FormatPlaceholders(m_TestSuiteFormatterBase.GetItem(13&), _
       InvokerContext.Member, _
       m_TestSuiteFormatterBase.FormatCallType(InvokerContext.CallType), _
       m_TestSuiteFormatterBase.FormatVariant(InvokerContext.Arguments), _
       InvokerContext.Check, _
       m_TestSuiteFormatterBase.FormatVariant(InvokerContext.Expected), _
       m_TestSuiteFormatterBase.FormatVariant(InvokerContext.Actual), _
       m_TestSuiteFormatterBase.FormatVariant(InvokerContext.CallTime, , False), _
       IIf(InvokerContext.HasFailed, COLOR_RED, COLOR_GREEN), _
       m_TestSuiteFormatterBase.FormatStatus(InvokerContext.HasFailed))

End Function
