VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteGeneratorLong"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteGenerator

'CONSTANTS

Private Const LONG_MAX As Double = 2147483647#
Private Const LONG_MIN As Double = &H80000000

'METHODS

Private Function ITestSuiteGenerator_GetValue(ByVal Length As Long) As Variant

    Randomize

    ITestSuiteGenerator_GetValue = CLng(LONG_MIN + Int((Abs(LONG_MAX - LONG_MIN) + 1) * Rnd))

End Function
