VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteAssert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteAssert
Implements ITestSuiteConstructible

'VARIABLES

Private m_TestSuiteCallback As ITestSuiteCallback
Private m_UtilityComparer As IUtilityComparer

'EVENTS

Private Function ITestSuiteConstructible_Construct(Callback As ITestSuiteCallback, Test As ITest) As ITestSuiteConstructible

    Class_Terminate

    Set m_TestSuiteCallback = Callback
    Set m_UtilityComparer = New UtilityComparer

    Set ITestSuiteConstructible_Construct = Me

End Function

Private Sub Class_Terminate()

    Set m_TestSuiteCallback = Nothing
    Set m_UtilityComparer = Nothing

End Sub

'METHODS

Private Function ITestSuiteAssert_Equals(Value As Variant) As ITestSuiteAction

    Set ITestSuiteAssert_Equals = GetAssertResult("Equals", Value, True, COMPARISON_EQUAL)

End Function

Private Function ITestSuiteAssert_Greater(Value As Variant) As ITestSuiteAction

    Set ITestSuiteAssert_Greater = GetAssertResult("Greater", Value, True, COMPARISON_GREATER)

End Function

Private Function ITestSuiteAssert_GreaterOrEquals(Value As Variant) As ITestSuiteAction

    Set ITestSuiteAssert_GreaterOrEquals = GetAssertResult("GreaterOrEquals", Value, True, COMPARISON_EQUAL, COMPARISON_GREATER)

End Function

Private Function ITestSuiteAssert_IsArray() As ITestSuiteAction

    Dim l_Check As String

    l_Check = "IsArray"

    If IsArray(m_TestSuiteCallback.Invoker.Context.Actual) Then

        Set ITestSuiteAssert_IsArray = GetAssertResult(l_Check, m_TestSuiteCallback.Invoker.Context.Actual, False, COMPARISON_EQUAL)

    Else

        Set ITestSuiteAssert_IsArray = GetAssertResult(l_Check, Empty, False)

    End If

End Function

Private Function ITestSuiteAssert_IsEmpty() As ITestSuiteAction

    Set ITestSuiteAssert_IsEmpty = GetAssertResult("IsEmpty", Empty, False, COMPARISON_EQUAL)

End Function

Private Function ITestSuiteAssert_IsFalse() As ITestSuiteAction

    Set ITestSuiteAssert_IsFalse = GetAssertResult("IsFalse", False, False, COMPARISON_EQUAL)

End Function

Private Function ITestSuiteAssert_IsNull() As ITestSuiteAction

    Set ITestSuiteAssert_IsNull = GetAssertResult("IsNull", Null, False, COMPARISON_EQUAL)

End Function

Private Function ITestSuiteAssert_IsObject() As ITestSuite

    Set ITestSuiteAssert_IsObject = GetAssertResultForObject("IsObject", vbNullString, False)

End Function

Private Function ITestSuiteAssert_IsObjectOfType(ObjectType As String) As ITestSuite

    Set ITestSuiteAssert_IsObjectOfType = GetAssertResultForObject("IsObjectOfType", ObjectType, True)

End Function

Private Function ITestSuiteAssert_IsTrue() As ITestSuiteAction

    Set ITestSuiteAssert_IsTrue = GetAssertResult("IsTrue", True, False, COMPARISON_EQUAL)

End Function

Private Function ITestSuiteAssert_Less(Value As Variant) As ITestSuiteAction

    Set ITestSuiteAssert_Less = GetAssertResult("Less", Value, True, COMPARISON_LESS)

End Function

Private Function ITestSuiteAssert_LessOrEquals(Value As Variant) As ITestSuiteAction

    Set ITestSuiteAssert_LessOrEquals = GetAssertResult("LessOrEquals", Value, True, COMPARISON_EQUAL, COMPARISON_LESS)

End Function

Private Function ITestSuiteAssert_NotEquals(Value As Variant) As ITestSuiteAction

    Set ITestSuiteAssert_NotEquals = GetAssertResult("NotEquals", Value, True, COMPARISON_GREATER, COMPARISON_LESS)

End Function

Private Function ITestSuiteAssert_NotNull() As ITestSuiteAction

    Set ITestSuiteAssert_NotNull = GetAssertResult("NotNull", Null, False, COMPARISON_GREATER, COMPARISON_LESS)

End Function

Private Function ITestSuiteAssert_Throws(ByVal ErrorNumber As Long) As ITestSuiteAction

    Set ITestSuiteAssert_Throws = GetAssertResultForError("Throws", ErrorNumber, True, COMPARISON_EQUAL)

End Function

Private Function ITestSuiteAssert_ThrowsAny() As ITestSuiteAction

    Set ITestSuiteAssert_ThrowsAny = GetAssertResultForError("ThrowsAny", 0&, False, COMPARISON_GREATER, COMPARISON_LESS)

End Function

Private Function ITestSuiteAssert_ThrowsNothing() As ITestSuiteAction

    Set ITestSuiteAssert_ThrowsNothing = GetAssertResultForError("ThrowsNothing", 0&, False, COMPARISON_EQUAL)

End Function

Private Function GetAssertResult(ByRef Check As String, ByRef Value As Variant, ByVal HasOutput As Boolean, ParamArray Expected() As Variant) As ITestSuiteAction

    Dim l_HasFailed As Boolean

    If m_TestSuiteCallback.Invoker.Context.ErrNumber Then

        l_HasFailed = True

        m_TestSuiteCallback.Invoker.Context.Actual = m_TestSuiteCallback.Invoker.Context.ErrDescription

    Else

        l_HasFailed = Not IsComparisonMatches(Value, CVar(Expected))

    End If

    PerformCallback Check, l_HasFailed, Value, HasOutput

    Set GetAssertResult = m_TestSuiteCallback

End Function

Private Function GetAssertResultForError(ByRef Check As String, ByRef Value As Variant, ByVal HasOutput As Boolean, ParamArray Expected() As Variant) As ITestSuiteAction

    Dim l_HasFailed As Boolean

    m_TestSuiteCallback.Invoker.Context.Actual = m_TestSuiteCallback.Invoker.Context.ErrNumber

    l_HasFailed = Not IsComparisonMatches(Value, CVar(Expected))

    PerformCallback Check, l_HasFailed, Value, HasOutput

    Set GetAssertResultForError = m_TestSuiteCallback

End Function

Private Function GetAssertResultForObject(ByRef Check As String, ByRef Value As Variant, ByVal HasOutput As Boolean) As ITestSuite

    Dim l_TestSuite As ITestSuiteConstructible
    Dim l_HasFailed As Boolean
    Dim l_Test As Test

    If IsObject(m_TestSuiteCallback.Invoker.Context.Actual) Then l_HasFailed = m_TestSuiteCallback.Invoker.Context.Actual Is Nothing Else l_HasFailed = True

    If l_HasFailed = False And Len(Value) > 0& Then l_HasFailed = Not m_UtilityComparer.AreEqual(TypeName(m_TestSuiteCallback.Invoker.Context.Actual), Value)

    PerformCallback Check, l_HasFailed, Value, HasOutput

    Set l_TestSuite = New TestSuite
    Set l_Test = New Test

    l_Test.Object = m_TestSuiteCallback.Invoker.Context.Actual

    Set GetAssertResultForObject = l_TestSuite.Construct(m_TestSuiteCallback, l_Test)

    Set l_Test = Nothing
    Set l_TestSuite = Nothing

End Function

Private Function IsComparisonMatches(ByRef Value As Variant, ByRef Expected As Variant) As Boolean

    Dim l_CompareResult As Long
    Dim l_Iterator As Long
    Dim l_UBound As Long

    l_UBound = UBound(Expected)

    If l_UBound >= 0& Then

        l_CompareResult = m_UtilityComparer.Compare(m_TestSuiteCallback.Invoker.Context.Actual, Value, vbBinaryCompare)

        For l_Iterator = 0& To l_UBound

            If l_CompareResult = Expected(l_Iterator) Then

                IsComparisonMatches = True

                Exit For

            End If

        Next l_Iterator

    End If

End Function

Private Sub PerformCallback(ByRef Check As String, ByVal Failed As Boolean, ByRef Expected As Variant, ByVal HasOutput As Boolean)

    With m_TestSuiteCallback.Invoker.Context
        .Check = Check
        .Expected = IIf(HasOutput, Expected, Empty)
        .HasFailed = Failed
    End With

    m_TestSuiteCallback.Callback

End Sub
