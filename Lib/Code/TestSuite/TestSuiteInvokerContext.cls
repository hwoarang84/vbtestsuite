VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteInvokerContext"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteInvokerContext

'VARIABLES

Private m_Actual As Variant
Private m_Arguments As Variant
Private m_CallTime As Double
Private m_CallType As Long
Private m_Check As String
Private m_ErrDescription As String
Private m_ErrNumber As Long
Private m_Expected As Variant
Private m_HasFailed As Boolean
Private m_Member As String

'EVENTS

Private Sub Class_Terminate()

    If IsObject(m_Actual) Then Set m_Actual = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteInvokerContext_Actual() As Variant

    If IsObject(m_Actual) Then Set ITestSuiteInvokerContext_Actual = m_Actual Else ITestSuiteInvokerContext_Actual = m_Actual

End Property

Private Property Let ITestSuiteInvokerContext_Actual(RHS As Variant)

    If IsObject(RHS) Then Set m_Actual = RHS Else m_Actual = RHS

End Property

Private Property Get ITestSuiteInvokerContext_Arguments() As Variant

    ITestSuiteInvokerContext_Arguments = m_Arguments

End Property

Private Property Let ITestSuiteInvokerContext_Arguments(RHS As Variant)

    m_Arguments = RHS

End Property

Private Property Get ITestSuiteInvokerContext_CallTime() As Double

    ITestSuiteInvokerContext_CallTime = m_CallTime

End Property

Private Property Let ITestSuiteInvokerContext_CallTime(ByVal RHS As Double)

    m_CallTime = RHS

End Property

Private Property Get ITestSuiteInvokerContext_CallType() As Long

    ITestSuiteInvokerContext_CallType = m_CallType

End Property

Private Property Let ITestSuiteInvokerContext_CallType(ByVal RHS As Long)

    m_CallType = RHS

End Property

Private Property Get ITestSuiteInvokerContext_Check() As String

    ITestSuiteInvokerContext_Check = m_Check

End Property

Private Property Let ITestSuiteInvokerContext_Check(RHS As String)

    m_Check = RHS

End Property

Private Property Get ITestSuiteInvokerContext_ErrDescription() As String

    ITestSuiteInvokerContext_ErrDescription = m_ErrDescription

End Property

Private Property Let ITestSuiteInvokerContext_ErrDescription(RHS As String)

    m_ErrDescription = RHS

End Property

Private Property Get ITestSuiteInvokerContext_ErrNumber() As Long

    ITestSuiteInvokerContext_ErrNumber = m_ErrNumber

End Property

Private Property Let ITestSuiteInvokerContext_ErrNumber(RHS As Long)

    m_ErrNumber = RHS

End Property

Private Property Get ITestSuiteInvokerContext_Expected() As Variant

    ITestSuiteInvokerContext_Expected = m_Expected

End Property

Private Property Let ITestSuiteInvokerContext_Expected(RHS As Variant)

    m_Expected = RHS

End Property

Private Property Get ITestSuiteInvokerContext_HasFailed() As Boolean

    ITestSuiteInvokerContext_HasFailed = m_HasFailed

End Property

Private Property Let ITestSuiteInvokerContext_HasFailed(ByVal RHS As Boolean)

    m_HasFailed = RHS

End Property

Private Property Get ITestSuiteInvokerContext_Member() As String

    ITestSuiteInvokerContext_Member = m_Member

End Property

Private Property Let ITestSuiteInvokerContext_Member(RHS As String)

    m_Member = RHS

End Property
