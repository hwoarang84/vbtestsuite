VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteStatics"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'CONSTANTS

Private Const DIMS_COUNT As Long = 1&
Private Const ELEMENTS_COUNT As Long = 10&

'VARIABLES

Private m_TestSuiteGeneratorBoolean As ITestSuiteGenerator
Private m_TestSuiteGeneratorByte As ITestSuiteGenerator
Private m_TestSuiteGeneratorCurrency As ITestSuiteGenerator
Private m_TestSuiteGeneratorDate As ITestSuiteGenerator
Private m_TestSuiteGeneratorDouble As ITestSuiteGenerator
Private m_TestSuiteGeneratorInteger As ITestSuiteGenerator
Private m_TestSuiteGeneratorLong As ITestSuiteGenerator
Private m_TestSuiteGeneratorSingle As ITestSuiteGenerator
Private m_TestSuiteGeneratorString As ITestSuiteGenerator

'EVENTS

Private Sub Class_Terminate()

    Set m_TestSuiteGeneratorBoolean = Nothing
    Set m_TestSuiteGeneratorByte = Nothing
    Set m_TestSuiteGeneratorCurrency = Nothing
    Set m_TestSuiteGeneratorDate = Nothing
    Set m_TestSuiteGeneratorDouble = Nothing
    Set m_TestSuiteGeneratorInteger = Nothing
    Set m_TestSuiteGeneratorLong = Nothing
    Set m_TestSuiteGeneratorSingle = Nothing
    Set m_TestSuiteGeneratorString = Nothing

End Sub

'METHODS

Public Function AnyArrayOfBoolean(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Boolean()

    AnyArrayOfBoolean = AnyArrayOf(GetGeneratorOfBoolean, vbBoolean, Dimensions, Elements)

End Function

Public Function AnyArrayOfByte(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Byte()

    AnyArrayOfByte = AnyArrayOf(GetGeneratorOfByte, vbByte, Dimensions, Elements)

End Function

Public Function AnyArrayOfCurrency(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Currency()

    AnyArrayOfCurrency = AnyArrayOf(GetGeneratorOfCurrency, vbCurrency, Dimensions, Elements)

End Function

Public Function AnyArrayOfDate(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Date()

    AnyArrayOfDate = AnyArrayOf(GetGeneratorOfDate, vbDate, Dimensions, Elements)

End Function

Public Function AnyArrayOfDouble(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Double()

    AnyArrayOfDouble = AnyArrayOf(GetGeneratorOfDouble, vbDouble, Dimensions, Elements)

End Function

Public Function AnyArrayOfInteger(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Integer()

    AnyArrayOfInteger = AnyArrayOf(GetGeneratorOfInteger, vbInteger, Dimensions, Elements)

End Function

Public Function AnyArrayOfLong(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Long()

    AnyArrayOfLong = AnyArrayOf(GetGeneratorOfLong, vbLong, Dimensions, Elements)

End Function

Public Function AnyArrayOfSingle(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Single()

    AnyArrayOfSingle = AnyArrayOf(GetGeneratorOfSingle, vbSingle, Dimensions, Elements)

End Function

Public Function AnyArrayOfString(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT, Optional ByVal Length As Long = ELEMENTS_COUNT) As String()

    AnyArrayOfString = AnyArrayOf(GetGeneratorOfString, vbString, Dimensions, Elements, Length)

End Function

Public Function AnyArrayOfVariant(Optional ByVal Dimensions As Long = DIMS_COUNT, Optional ByVal Elements As Long = ELEMENTS_COUNT) As Variant

    AnyArrayOfVariant = AnyArrayOf(GetGenerator, vbVariant, Dimensions, Elements)

End Function

Public Function AnyBoolean() As Boolean

    AnyBoolean = GetGeneratorOfBoolean.GetValue(0&)

End Function

Public Function AnyByte() As Byte

    AnyByte = GetGeneratorOfByte.GetValue(0&)

End Function

Public Function AnyCurrency() As Currency

    AnyCurrency = GetGeneratorOfCurrency.GetValue(0&)

End Function

Public Function AnyDate() As Date

    AnyDate = GetGeneratorOfDate.GetValue(0&)

End Function

Public Function AnyDouble() As Double

    AnyDouble = GetGeneratorOfDouble.GetValue(0&)

End Function

Public Function AnyInteger() As Integer

    AnyInteger = GetGeneratorOfInteger.GetValue(0&)

End Function

Public Function AnyLong() As Long

    AnyLong = GetGeneratorOfLong.GetValue(0&)

End Function

Public Function AnySingle() As Single

    AnySingle = GetGeneratorOfSingle.GetValue(0&)

End Function

Public Function AnyString(Optional ByVal Length As Long = ELEMENTS_COUNT) As String

    AnyString = GetGeneratorOfString.GetValue(Length)

End Function

Public Function AnyVariant() As Variant

    AnyVariant = GetGenerator.GetValue(0&)

End Function

Private Function AnyArrayOf(ByRef Generator As ITestSuiteGenerator, ByVal VarType As VbVarType, ByVal Dims As Long, ByVal Elements As Long, Optional ByVal Length As Long) As Variant

    Dim l_Array As TSAFEARRAY
    Dim l_Bounds() As TSAFEARRAYBOUND
    Dim l_Dims As Long
    Dim l_Indexes() As Long
    Dim l_Iterator As Long

    l_Dims = Dims - 1&

    ReDim l_Bounds(l_Dims)
    ReDim l_Indexes(l_Dims)

    For l_Iterator = 0& To l_Dims

        If l_Iterator < l_Dims Then l_Bounds(l_Iterator).lElements = 1& Else l_Bounds(l_Dims).lElements = Elements

    Next l_Iterator

    If SafeArray_Create(l_Array, VarType, Dims, l_Bounds) = S_OK Then

        For l_Iterator = 0& To Elements - 1&

            l_Indexes(l_Dims) = l_Iterator

            SafeArray_SetElement l_Array, l_Indexes(0), Generator.GetValue(Length)

        Next l_Iterator

        SafeArray_Copy l_Array, AnyArrayOf
        SafeArray_Destroy l_Array

        Erase l_Bounds
        Erase l_Indexes

    End If

End Function

Private Function GetGenerator() As ITestSuiteGenerator

    Randomize

    Select Case CLng(Int(9& * Rnd))

        Case 0&: Set GetGenerator = GetGeneratorOfBoolean

        Case 1&: Set GetGenerator = GetGeneratorOfByte

        Case 2&: Set GetGenerator = GetGeneratorOfCurrency

        Case 3&: Set GetGenerator = GetGeneratorOfDate

        Case 4&: Set GetGenerator = GetGeneratorOfDouble

        Case 5&: Set GetGenerator = GetGeneratorOfInteger

        Case 6&: Set GetGenerator = GetGeneratorOfLong

        Case 7&: Set GetGenerator = GetGeneratorOfSingle

        Case Else: Set GetGenerator = GetGeneratorOfString

    End Select

End Function

Private Function GetGeneratorOfBoolean() As ITestSuiteGenerator

    If m_TestSuiteGeneratorBoolean Is Nothing Then Set m_TestSuiteGeneratorBoolean = New TestSuiteGeneratorBoolean

    Set GetGeneratorOfBoolean = m_TestSuiteGeneratorBoolean

End Function

Private Function GetGeneratorOfByte() As ITestSuiteGenerator

    If m_TestSuiteGeneratorByte Is Nothing Then Set m_TestSuiteGeneratorByte = New TestSuiteGeneratorByte

    Set GetGeneratorOfByte = m_TestSuiteGeneratorByte

End Function

Private Function GetGeneratorOfCurrency() As ITestSuiteGenerator

    If m_TestSuiteGeneratorCurrency Is Nothing Then Set m_TestSuiteGeneratorCurrency = New TestSuiteGeneratorCurrency

    Set GetGeneratorOfCurrency = m_TestSuiteGeneratorCurrency

End Function

Private Function GetGeneratorOfDate() As ITestSuiteGenerator

    If m_TestSuiteGeneratorDate Is Nothing Then Set m_TestSuiteGeneratorDate = New TestSuiteGeneratorDate

    Set GetGeneratorOfDate = m_TestSuiteGeneratorDate

End Function

Private Function GetGeneratorOfDouble() As ITestSuiteGenerator

    If m_TestSuiteGeneratorDouble Is Nothing Then Set m_TestSuiteGeneratorDouble = New TestSuiteGeneratorDouble

    Set GetGeneratorOfDouble = m_TestSuiteGeneratorDouble

End Function

Private Function GetGeneratorOfInteger() As ITestSuiteGenerator

    If m_TestSuiteGeneratorInteger Is Nothing Then Set m_TestSuiteGeneratorInteger = New TestSuiteGeneratorInteger

    Set GetGeneratorOfInteger = m_TestSuiteGeneratorInteger

End Function

Private Function GetGeneratorOfLong() As ITestSuiteGenerator

    If m_TestSuiteGeneratorLong Is Nothing Then Set m_TestSuiteGeneratorLong = New TestSuiteGeneratorLong

    Set GetGeneratorOfLong = m_TestSuiteGeneratorLong

End Function

Private Function GetGeneratorOfSingle() As ITestSuiteGenerator

    If m_TestSuiteGeneratorSingle Is Nothing Then Set m_TestSuiteGeneratorSingle = New TestSuiteGeneratorSingle

    Set GetGeneratorOfSingle = m_TestSuiteGeneratorSingle

End Function

Private Function GetGeneratorOfString() As ITestSuiteGenerator

    If m_TestSuiteGeneratorString Is Nothing Then Set m_TestSuiteGeneratorString = New TestSuiteGeneratorString

    Set GetGeneratorOfString = m_TestSuiteGeneratorString

End Function
