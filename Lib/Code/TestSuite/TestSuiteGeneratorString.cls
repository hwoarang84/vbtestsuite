VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteGeneratorString"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteGenerator

'CONSTANTS

Private Const STRING_CHARS As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
Private Const STRING_CHARS_LENGTH As Long = 10&

'METHODS

Private Function ITestSuiteGenerator_GetValue(ByVal Length As Long) As Variant

    Dim l_Chars() As Byte
    Dim l_CharsCount As Long
    Dim l_Iterator As Long
    Dim l_Length As Long
    Dim l_Value() As Byte

    Randomize

    l_Chars = STRING_CHARS
    l_CharsCount = Len(STRING_CHARS) - 1&
    l_Length = (IIf(Length, Length, STRING_CHARS_LENGTH) * 2&) - 1&

    ReDim l_Value(l_Length) As Byte

    For l_Iterator = 0& To l_Length Step 2&
        l_Value(l_Iterator) = l_Chars(CLng(l_CharsCount * Rnd) * 2&)
    Next l_Iterator

    ITestSuiteGenerator_GetValue = CStr(l_Value)

    Erase l_Value

End Function
