VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteFormatterBase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'CONSTANTS

Private Const ARRAY_ITEMS_LIMIT As Long = 20&

'VARIABLES

Private m_TemplateBody As String
Private m_TemplateFooter As String
Private m_TemplateHeader As String
Private m_TemplateItem As String
Private m_UtilityResource As IUtilityResource

'EVENTS

Private Sub Class_Initialize()

    Set m_UtilityResource = New UtilityResource

End Sub

Private Sub Class_Terminate()

    Set m_UtilityResource = Nothing

End Sub

'METHODS

Public Function FormatCallType(ByVal CallType As VbCallType) As String

    Select Case CallType

        Case VbGet: FormatCallType = "Get"

        Case VbLet: FormatCallType = "Let"

        Case VbSet: FormatCallType = "Set"

        Case VbMethod: FormatCallType = "Method"

    End Select

End Function

Public Function FormatStatus(ByVal Value As Boolean) As String

    If Value Then FormatStatus = "Failed" Else FormatStatus = "Success"

End Function

Public Function FormatVariant(ByRef Value As Variant, Optional ByVal AppendQuotes As Boolean, Optional ByVal AppendType As Boolean = True) As String

    Dim l_Array As TSAFEARRAY
    Dim l_Iterator As Long
    Dim l_Result As String

    If IsObject(Value) Then

        l_Result = "Object"

    ElseIf IsEmpty(Value) Then

        l_Result = "Empty"

    ElseIf IsNull(Value) Then

        l_Result = "Null"

    ElseIf IsArray(Value) Then

        SafeArray_FillInfo l_Array, VarPtr(Value)

        If l_Array.iDims = 1 Then

            For l_Iterator = LBound(Value) To UBound(Value)

                If l_Iterator < ARRAY_ITEMS_LIMIT Then

                    l_Result = l_Result & FormatVariant(Value(l_Iterator), AppendQuotes, AppendType) & ChrW$(44&)

                Else

                    l_Result = l_Result & FormatVariant(String$(3&, ChrW$(46&)), AppendQuotes, AppendType) & ChrW$(44&)

                    Exit For

                End If

            Next l_Iterator

            FormatVariant = ChrW$(91&) & Left$(l_Result, Len(l_Result) - 1&) & ChrW$(93&)

            Exit Function

        ElseIf l_Array.iDims > 1 Then

            l_Result = l_Array.iDims & "-dimensional array"

        Else

            l_Result = "Empty array"

        End If

    ElseIf IsNumeric(Value) Then

        l_Result = Trim$(Str$(Value))

    Else

        l_Result = Value

    End If

    If AppendType Then l_Result = l_Result & ChrW$(32&) & ChrW$(40&) & TypeName(Value) & ChrW$(41&)

    If AppendQuotes Then FormatVariant = ChrW$(34&) & l_Result & ChrW$(34&) Else FormatVariant = l_Result

End Function

Public Function GetBody(ByVal ResourceId As Long) As String

    GetBody = GetCacheOrResource(ResourceId, m_TemplateBody)

End Function

Public Function GetFooter(ByVal ResourceId As Long) As String

    GetFooter = GetCacheOrResource(ResourceId, m_TemplateFooter)

End Function

Public Function GetHeader(ByVal ResourceId As Long) As String

    GetHeader = GetCacheOrResource(ResourceId, m_TemplateHeader)

End Function

Public Function GetItem(ByVal ResourceId As Long) As String

    GetItem = GetCacheOrResource(ResourceId, m_TemplateItem)

End Function

Private Function GetCacheOrResource(ByVal ResourceId As Long, ByRef TemplateItem As String) As String

    If Len(TemplateItem) = 0& Then TemplateItem = m_UtilityResource.GetUTF8Content(ResourceId)

    GetCacheOrResource = TemplateItem

End Function
