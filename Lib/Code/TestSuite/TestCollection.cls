VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestCollection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'ENUMERATIONS

Public Enum OUTPUT_FORMAT
    HTML&
    JSON&
End Enum

'VARIABLES

Private m_TestCollection As Collection
Private m_TestSuiteRecorder As ITestSuiteRecorder

'EVENTS

Private Sub Class_Initialize()

    Set m_TestCollection = New Collection
    Set m_TestSuiteRecorder = New TestSuiteRecorder

End Sub

Private Sub Class_Terminate()

    Set m_TestCollection = Nothing
    Set m_TestSuiteRecorder = Nothing

End Sub

'METHODS

Public Sub Add(ByRef Test As ITest)

    If Test Is Nothing Then Throw ERR_ID_TEST_NOTHING, ERR_MSG_TEST_NOTHING

    If Test.Object Is Nothing Then Throw ERR_ID_TEST_OBJECT_NOTHING, ERR_MSG_TEST_OBJECT_NOTHING, TypeName(Test)

    m_TestCollection.Add Test

End Sub

Public Sub Clear()

    Set m_TestCollection = Nothing
    Set m_TestCollection = New Collection

End Sub

Public Sub Run(Optional ByVal Format As OUTPUT_FORMAT, Optional ByRef FileName As String)

    If m_TestCollection.Count Then m_TestSuiteRecorder.Record m_TestCollection, Format, FileName Else Throw ERR_ID_TESTS_COLLECTION_EMPTY, ERR_MSG_TESTS_COLLECTION_EMPTY

End Sub
