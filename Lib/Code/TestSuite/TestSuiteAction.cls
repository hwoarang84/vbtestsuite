VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteAction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteAction
Implements ITestSuiteCallback
Implements ITestSuiteConstructible

'VARIABLES

Private m_TestSuiteAssert As ITestSuiteConstructible
Private m_TestSuiteCallback As ITestSuiteCallback
Private m_Test As ITest

'EVENTS

Private Function ITestSuiteConstructible_Construct(Callback As ITestSuiteCallback, Test As ITest) As ITestSuiteConstructible

    Class_Terminate

    Set m_TestSuiteAssert = New TestSuiteAssert
    Set m_TestSuiteCallback = Callback
    Set m_Test = Test

    Set ITestSuiteConstructible_Construct = Me

End Function

Private Sub Class_Terminate()

    Set m_TestSuiteAssert = Nothing
    Set m_TestSuiteCallback = Nothing
    Set m_Test = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteCallback_Invoker() As ITestSuiteInvoker

    Set ITestSuiteCallback_Invoker = m_TestSuiteCallback.Invoker

End Property

'METHODS

Private Function ITestSuiteAction_Assert(ParamArray Arguments() As Variant) As ITestSuiteAssert

    m_Test.SetUp m_TestSuiteCallback.Invoker.Context.Member, m_TestSuiteCallback.Invoker.Context.CallType

    m_TestSuiteCallback.Invoker.Invoke m_Test.Object, CVar(Arguments)

    m_Test.TearDown m_TestSuiteCallback.Invoker.Context.Member, m_TestSuiteCallback.Invoker.Context.CallType

    Set ITestSuiteAction_Assert = m_TestSuiteAssert.Construct(Me, Nothing)

End Function

Private Sub ITestSuiteCallback_Callback()

    m_TestSuiteCallback.Callback

End Sub
