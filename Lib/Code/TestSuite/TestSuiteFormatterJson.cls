VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteFormatterJson"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteFormatter

'CONSTANTS

Private Const DOUBLE_QUOTE As String = """"
Private Const LEFT_SLASH As String = "\"

'VARIABLES

Private m_TestSuiteFormatterBase As TestSuiteFormatterBase

'EVENTS

Private Sub Class_Initialize()

    Set m_TestSuiteFormatterBase = New TestSuiteFormatterBase

End Sub

Private Sub Class_Terminate()

    Set m_TestSuiteFormatterBase = Nothing

End Sub

'PROPERTIES

Private Property Get ITestSuiteFormatter_Body() As String

    ITestSuiteFormatter_Body = m_TestSuiteFormatterBase.GetBody(22&)

End Property

Private Property Get ITestSuiteFormatter_Extension() As String

    ITestSuiteFormatter_Extension = ".json"

End Property

Private Property Get ITestSuiteFormatter_Footer() As String

    ITestSuiteFormatter_Footer = m_TestSuiteFormatterBase.GetFooter(21&)

End Property

Private Property Get ITestSuiteFormatter_Header() As String

    ITestSuiteFormatter_Header = m_TestSuiteFormatterBase.GetHeader(20&)

End Property

Private Property Get ITestSuiteFormatter_Separator() As String

    ITestSuiteFormatter_Separator = ChrW$(44&)

End Property

'METHODS

Private Function ITestSuiteFormatter_Format(InvokerContext As ITestSuiteInvokerContext) As String

    ITestSuiteFormatter_Format = FormatPlaceholders(m_TestSuiteFormatterBase.GetItem(23&), _
       InvokerContext.Member, _
       m_TestSuiteFormatterBase.FormatCallType(InvokerContext.CallType), _
       FormatVariantWithEscape(InvokerContext.Arguments), _
       InvokerContext.Check, _
       FormatVariantWithEscape(InvokerContext.Expected), _
       FormatVariantWithEscape(InvokerContext.Actual), _
       m_TestSuiteFormatterBase.FormatVariant(InvokerContext.CallTime, , False), _
       m_TestSuiteFormatterBase.FormatStatus(InvokerContext.HasFailed))

End Function

Private Function FormatVariantWithEscape(ByRef vValue As Variant) As String

    Dim l_Value As String

    l_Value = Replace(Replace(m_TestSuiteFormatterBase.FormatVariant(vValue), LEFT_SLASH, LEFT_SLASH & LEFT_SLASH), DOUBLE_QUOTE, LEFT_SLASH & DOUBLE_QUOTE)

    FormatVariantWithEscape = m_TestSuiteFormatterBase.FormatVariant(l_Value, True, False)

End Function
