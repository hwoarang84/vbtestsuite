VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITestSuiteInvoker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get Context() As ITestSuiteInvokerContext
    '
End Property

'METHODS

Public Sub GetMember(ByRef Object As Object, ByRef Member As String, ByVal CallType As Long)
    '
End Sub

Public Sub Invoke(ByRef Object As Object, ByRef Arguments As Variant)
    '
End Sub
