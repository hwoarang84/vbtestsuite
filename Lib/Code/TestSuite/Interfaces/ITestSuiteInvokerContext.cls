VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITestSuiteInvokerContext"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get Actual() As Variant
    '
End Property

Public Property Let Actual(ByRef NewValue As Variant)
    '
End Property

Public Property Get Arguments() As Variant
    '
End Property

Public Property Let Arguments(ByRef NewValue As Variant)
    '
End Property

Public Property Get CallTime() As Double
    '
End Property

Public Property Let CallTime(ByVal NewValue As Double)
    '
End Property

Public Property Get CallType() As Long
    '
End Property

Public Property Let CallType(ByVal NewValue As Long)
    '
End Property

Public Property Get Check() As String
    '
End Property

Public Property Let Check(ByRef NewValue As String)
    '
End Property

Public Property Get ErrDescription() As String
    '
End Property

Public Property Let ErrDescription(ByRef NewValue As String)
    '
End Property

Public Property Get ErrNumber() As Long
    '
End Property

Public Property Let ErrNumber(ByRef NewValue As Long)
    '
End Property

Public Property Get Expected() As Variant
    '
End Property

Public Property Let Expected(ByRef NewValue As Variant)
    '
End Property

Public Property Get HasFailed() As Boolean
    '
End Property

Public Property Let HasFailed(ByVal NewValue As Boolean)
    '
End Property

Public Property Get Member() As String
    '
End Property

Public Property Let Member(ByRef NewValue As String)
    '
End Property
