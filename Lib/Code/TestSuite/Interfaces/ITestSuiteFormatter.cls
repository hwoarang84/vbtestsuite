VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITestSuiteFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get Body() As String
    '
End Property

Public Property Get Extension() As String
    '
End Property

Public Property Get Footer() As String
    '
End Property

Public Property Get Header() As String
    '
End Property

Public Property Get Separator() As String
    '
End Property

'METHODS

Public Function Format(ByRef InvokerContext As ITestSuiteInvokerContext) As String
    '
End Function
