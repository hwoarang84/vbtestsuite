VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITestSuiteAssert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'METHODS

Public Function Equals(ByRef Value As Variant) As ITestSuiteAction
    '
End Function

Public Function Greater(ByRef Value As Variant) As ITestSuiteAction
    '
End Function

Public Function GreaterOrEquals(ByRef Value As Variant) As ITestSuiteAction
    '
End Function

Public Function IsArray() As ITestSuiteAction
    '
End Function

Public Function IsEmpty() As ITestSuiteAction
    '
End Function

Public Function IsFalse() As ITestSuiteAction
    '
End Function

Public Function IsNull() As ITestSuiteAction
    '
End Function

Public Function IsObject() As ITestSuite
    '
End Function

Public Function IsObjectOfType(ByRef ObjectType As String) As ITestSuite
    '
End Function

Public Function IsTrue() As ITestSuiteAction
    '
End Function

Public Function Less(ByRef Value As Variant) As ITestSuiteAction
    '
End Function

Public Function LessOrEquals(ByRef Value As Variant) As ITestSuiteAction
    '
End Function

Public Function NotEquals(ByRef Value As Variant) As ITestSuiteAction
    '
End Function

Public Function NotNull() As ITestSuiteAction
    '
End Function

Public Function Throws(ByVal ErrorNumber As Long) As ITestSuiteAction
    '
End Function

Public Function ThrowsAny() As ITestSuiteAction
    '
End Function

Public Function ThrowsNothing() As ITestSuiteAction
    '
End Function
