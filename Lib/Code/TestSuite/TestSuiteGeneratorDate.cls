VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TestSuiteGeneratorDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITestSuiteGenerator

'CONSTANTS

Private Const DATE_MAX As Double = 2958466
Private Const DATE_MIN As Double = -657434

'METHODS

Private Function ITestSuiteGenerator_GetValue(ByVal Length As Long) As Variant

    Randomize

    ITestSuiteGenerator_GetValue = CLng(DATE_MIN + Int((Abs(DATE_MAX - DATE_MIN) + 1) * Rnd))

End Function
