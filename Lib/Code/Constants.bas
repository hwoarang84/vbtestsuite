Attribute VB_Name = "Constants"
Option Explicit

'CONSTANTS

Public Const ERR_ID_FILESYSTEM_CREATE As Long = vbObjectError + 6&
Public Const ERR_ID_FILESYSTEM_DELETE As Long = vbObjectError + 7&
Public Const ERR_ID_FILESYSTEM_OPEN As Long = vbObjectError + 8&
Public Const ERR_ID_FILESYSTEM_WRITE As Long = vbObjectError + 9&
Public Const ERR_ID_TEST_NOTHING As Long = vbObjectError + 1&
Public Const ERR_ID_TEST_OBJECT_NOTHING As Long = vbObjectError + 2&
Public Const ERR_ID_TESTS_COLLECTION_EMPTY As Long = vbObjectError + 3&
Public Const ERR_ID_TLI_APPLICATION_CREATE As Long = vbObjectError + 4&
Public Const ERR_ID_TLI_TESTABLE_OBJECT_NOTHING As Long = vbObjectError + 5&
Public Const ERR_MSG_ADDIN_NOPROJECT As String = "Standard EXE type project is not found in solution"
Public Const ERR_MSG_ADDIN_PROJECT_NOREFERENCE As String = "None of Standard EXE type project existing in solution has '{0}' library reference"
Public Const ERR_MSG_ADDIN_PROJECT_NOTESTS As String = "Standard EXE type project referenced to '{0}' library should have at least one class module that implements '{0}.ITest' interface"
Public Const ERR_MSG_FILESYSTEM_CREATE As String = "Can not create '{0}' file"
Public Const ERR_MSG_FILESYSTEM_DELETE As String = "Can not delete '{0}' file"
Public Const ERR_MSG_FILESYSTEM_OPEN As String = "Can not open '{0}' file"
Public Const ERR_MSG_FILESYSTEM_WRITE As String = "Can not write '{0}' file"
Public Const ERR_MSG_TEST_NOTHING As String = "Test is not set"
Public Const ERR_MSG_TEST_OBJECT_NOTHING As String = "Test '{0}.Object' property is not set"
Public Const ERR_MSG_TESTS_COLLECTION_EMPTY As String = "Tests collection is empty"
Public Const ERR_MSG_TLI_APPLICATION_CREATE As String = "Can not create 'TLI.TLIApplicationClass' object"
Public Const ERR_MSG_TLI_TESTABLE_OBJECT_NOTHING As String = "Testable object is not set"
