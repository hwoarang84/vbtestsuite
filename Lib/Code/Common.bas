Attribute VB_Name = "Common"
Option Explicit

'METHODS

Public Function FormatPlaceholders(ByRef Target As String, ParamArray Arguments() As Variant) As String

    Dim l_Iterator As Long
    Dim l_Result As String

    l_Result = Target

    For l_Iterator = 0& To UBound(Arguments)
        l_Result = Replace(l_Result, ChrW$(123&) & l_Iterator & ChrW$(125&), Arguments(l_Iterator))
    Next l_Iterator

    FormatPlaceholders = l_Result

End Function

Public Sub Throw(ByVal Error As Long, ByRef Message As String, ParamArray Arguments() As Variant)

    If UBound(Arguments) > -1& Then Exception_Throw Error, App.FileDescription, Message, Arguments(0) Else Exception_Throw Error, App.FileDescription, Message

End Sub

Public Sub ThrowIfAny()

    Exception_Throw 0&, App.FileDescription, vbNullString

End Sub
