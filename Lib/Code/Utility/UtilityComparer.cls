VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UtilityComparer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IUtilityComparer

'PROPERTIES

Private Property Get IUtilityComparer_AreEqual(Value1 As Variant, Value2 As Variant) As Boolean

    IUtilityComparer_AreEqual = IUtilityComparer_Compare(Value1, Value2, vbBinaryCompare) = COMPARISON_EQUAL

End Property

'METHODS

Private Function IUtilityComparer_Compare(Value1 As Variant, Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    IUtilityComparer_Compare = Compare_Any(Value1, Value2, CompareMethod)

End Function
