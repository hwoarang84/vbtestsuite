VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UtilityTimer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IUtilityTimer

'VARIABLES

Private m_Frequency As Currency
Private m_FrequencyQueried As Boolean
Private m_Overhead As Currency
Private m_Start As Currency

'PROPERTIES

Private Property Get IUtilityTimer_Elapsed() As Double

    Dim l_Current As Currency

    Timer_QueryCounter l_Current

    On Error GoTo ErrorHandler

    IUtilityTimer_Elapsed = (l_Current - m_Start - m_Overhead) / m_Frequency

ErrorHandler:

End Property

'METHODS

Private Sub IUtilityTimer_Start()

    If Not m_FrequencyQueried Then m_FrequencyQueried = Timer_QueryFrequency(m_Frequency, m_Overhead)

    m_Start = 0@

    Timer_QueryCounter m_Start

End Sub
