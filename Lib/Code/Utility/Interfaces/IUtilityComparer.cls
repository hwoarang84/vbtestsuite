VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IUtilityComparer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'PROPERTIES

Public Property Get AreEqual(ByRef Value1 As Variant, ByRef Value2 As Variant) As Boolean
    '
End Property

'METHODS

Public Function Compare(ByRef Value1 As Variant, ByRef Value2 As Variant, ByVal CompareMethod As VbCompareMethod) As Long
    '
End Function
