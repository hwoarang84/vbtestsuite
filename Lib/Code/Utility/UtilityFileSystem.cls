VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UtilityFileSystem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IUtilityFileSystem

'VARIABLES

Private m_FileName As String
Private m_Handle As Long

'METHODS

Private Function IUtilityFileSystem_Create(FileName As String) As Boolean

    IUtilityFileSystem_Flush

    m_FileName = FileName

    m_Handle = FileSystem_Open(m_FileName, FILE_GENERIC_WRITE, FILE_SHARE_NONE, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL)

    If m_Handle Then IUtilityFileSystem_Create = True Else Throw ERR_ID_FILESYSTEM_CREATE, ERR_MSG_FILESYSTEM_CREATE, m_FileName

End Function

Private Function IUtilityFileSystem_Delete(FilePath As String) As Boolean

    If FileSystem_Delete(FilePath) Then IUtilityFileSystem_Delete = True Else Throw ERR_ID_FILESYSTEM_DELETE, ERR_MSG_FILESYSTEM_DELETE, FilePath

End Function

Private Function IUtilityFileSystem_Execute(FilePath As String) As Boolean

    If FileSystem_Execute(FilePath, EXECVERB_OPEN) > SE_ERR_DLLNOTFOUND Then IUtilityFileSystem_Execute = True Else Throw ERR_ID_FILESYSTEM_OPEN, ERR_MSG_FILESYSTEM_OPEN, FilePath

End Function

Private Sub IUtilityFileSystem_Flush()

    If m_Handle Then

        FileSystem_Close m_Handle

        m_Handle = 0&

    End If

End Sub

Private Function IUtilityFileSystem_WriteUTF8(Data As String) As Boolean

    Dim l_Bytes() As Byte
    Dim l_BytesLength As Long
    Dim l_DataLength As Long
    Dim l_DataPointer As Long
    Dim l_Result As Long

    On Error GoTo ErrorHandler

    l_DataLength = Len(Data)
    l_DataPointer = StrPtr(Data)
    l_BytesLength = String_ConvertToUTF8(l_DataPointer, l_DataLength, 0&, 0&)

    If l_BytesLength Then

        ReDim l_Bytes(l_BytesLength)

        If String_ConvertToUTF8(l_DataPointer, l_DataLength, VarPtr(l_Bytes(0)), l_BytesLength) Then l_Result = FileSystem_Write(m_Handle, VarPtr(l_Bytes(0)), l_BytesLength, 0&)

    End If

ErrorHandler:

    Erase l_Bytes

    If l_Result Then IUtilityFileSystem_WriteUTF8 = True Else Throw ERR_ID_FILESYSTEM_WRITE, ERR_MSG_FILESYSTEM_WRITE, m_FileName

End Function
