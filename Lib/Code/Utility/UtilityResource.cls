VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UtilityResource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements IUtilityResource

'VARIABLES

Private m_AppName As String

'EVENTS

Private Sub Class_Initialize()

    m_AppName = UCase$(App.EXEName)

End Sub

'METHODS

Private Function IUtilityResource_GetUTF8Content(ByVal ResourceId As Long) As String

    Dim l_Bytes() As Byte
    Dim l_BytesCount As Long
    Dim l_BytesPtr As Long
    Dim l_Length As Long
    Dim l_Utf16Result As String

    l_Bytes = LoadResData(ResourceId, m_AppName)
    l_BytesCount = UBound(l_Bytes) + 1&
    l_BytesPtr = VarPtr(l_Bytes(0))

    l_Length = String_ConvertFromUTF8(MB_ERR_INVALID_CHARS, l_BytesPtr, l_BytesCount, 0&, 0&)

    If l_Length Then

        l_Utf16Result = Space$(l_Length)

        If String_ConvertFromUTF8(0&, l_BytesPtr, l_BytesCount, StrPtr(l_Utf16Result), l_Length) Then IUtilityResource_GetUTF8Content = l_Utf16Result

    End If

    Erase l_Bytes

End Function
